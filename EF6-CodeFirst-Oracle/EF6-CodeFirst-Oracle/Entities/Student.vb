﻿Namespace Entities

    Public Class Student
        Public Property StudentID As Integer
        Public Property StudentName As String
        Public Property DateOfBirth As Date
        Public Property Photo As Byte()
        Public Property Height As Decimal
        Public Property Weight As Decimal

        Public Overridable Property Grade As Grade

    End Class

End Namespace