﻿Namespace Entities

    Public Class Grade
        Public Property GradeId As Integer
        Public Property GradeName As String
        Public Property Section As String

        Public Overridable Property Students As ICollection(Of Student)
    End Class

End Namespace