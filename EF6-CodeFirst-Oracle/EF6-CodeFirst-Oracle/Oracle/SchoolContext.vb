﻿Imports System.Data.Entity
Imports EF6_CodeFirst_Oracle.Entities

Namespace Oracle

    Public Class SchoolContext
        Inherits DbContext

        Public Sub New()
            MyBase.New("name=SchoolContext")
            Database.SetInitializer(New CreateDatabaseIfNotExists(Of SchoolContext))
            'Database.SetInitializer(New DropCreateDatabaseIfModelChanges(Of SchoolContext))
            'Database.SetInitializer(New DropCreateDatabaseAlways(Of SchoolContext))
            'Database.SetInitializer(New SchoolDBInitializer()) ' カスタム初期化
            'Database.SetInitializer(Of SchoolContext)(Nothing) ' 初期化なし
        End Sub

        Public Overridable Property Students As DbSet(Of Student)

        Public Overridable Property Grades As DbSet(Of Grade)

        Protected Overrides Sub OnModelCreating(modelBuilder As DbModelBuilder)
            modelBuilder.HasDefaultSchema("FREE")
        End Sub

    End Class

End Namespace