﻿Imports EF6_CodeFirst_Oracle.Entities
Imports EF6_CodeFirst_Oracle.Oracle

Module Module1

    Sub Main()
        Using ctx As New SchoolContext
            Dim stud As New Student With {.StudentName = "Bill"}
            ctx.Students.Add(stud)
            ctx.SaveChanges()
        End Using
    End Sub

End Module